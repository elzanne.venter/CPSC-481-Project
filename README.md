# CPSC 481 (Human-Computer Interaction 1) at the University of Calgary Term Group Project

### By: Elzanne Venter, Jona Grageda, Daniel Grande, Prithivi Raj

## Purpose
<div>The purpose of this project was to design and develop an electronic menu/ ordering system for a hypothetical restaurant, Mora's All You Can Eat Breakfast Cafe. This project provided an introduction to the process of designing user interfaces and creating computer applications from the generated designs. </div>

## Process
<div>The process of designing and implementing our system included the following steps
    <ol>
        <li>Identifying expected users and tasks.</li>
        <li>Creating a list of requirements based on the information gathered in step 1.</li>
        <li>Developing low fidelity prototypes (sketches).</li>
        <li>Performing a task-centered walkthrough to identify usability bugs and selecting a final sketch to develop further.</li>
        <li>Developing medium fidelity prototype (Adobe XD).</li>
        <li>Developing horizontal prototype (all features visible but not functional).</li>
        <li>Developing a vertical prototype (most features should be functional).</li>
        <li>Performing a final heuristic evaluation based on Jakob Nielson's heuristics for interface design.</li>
    </ol>
    </div>

## Sketches
<div>Demo video available [here](https://www.youtube.com/watch?v=gPEqLnTz_m4&ab_channel=ElzanneVenter) </div> 

## Adobe XD
<div>Demo video available [here](https://drive.google.com/file/d/1-xj2fBLTdYN6yjxwdyfkugpAUN96xew8/view?usp=sharing) </div> 

## Final Implementation
<div>Demo video available [here](https://drive.google.com/file/d/1pA2My9903bKm1H1MuzV_9fRX4edA-wC7/view?usp=sharing) </div>
