﻿using System;
using System.Collections.Generic;
namespace NewMoras
{
    public class Menu
    {
        public List<Item> menuItems = new List<Item>();

        //Pancakes and Waffles
        public Item originalPancakes = new Item("Original Pancakes", "images/food/pancakes.jpg", "Classic pancakes and maple syrup.", 400, "2 pancakes", new List<string> { "flour", "sugar", "milk" }, new List<string> { "dairy" }, "Pancakes and Waffles", 20, "");
        public Item blueberryPancakes = new Item("Blueberry Pancakes", "images/food/bpancakes.jpg", "Blueberry pancakes and maple syrup.", 500, "2 pancakes", new List<string> { "flour", "sugar", "milk", "blueberries" }, new List<string> { "dairy" }, "Pancakes and Waffles", 20, "");
        public Item originalWaffles = new Item("Original Waffles", "images/food/waffles.jpg", "Classic waffles with maple syrup and whipped cream.", 600, "2 waffles", new List<string> { "flour", "sugar", "milk" }, new List<string> { "dairy" }, "Pancakes and Waffles", 20, "");

        //sides
        public Item fruitParfait = new Item("Fruit Parfait", "images/food/fruitParfait.jpg", "Delicious assortment of fruits served with vanilla yogurt and granola.", 600, "1 bowl", new List<string> { "fruits", "granola", "yogurt" }, new List<string> { "dairy" }, "Sides", 10, "");
        public Item hashBrowns = new Item("Hash Browns", "images/food/hashbrowns.jpg", "Classic Hash Browns", 600, "1 small plate full", new List<string> { "potato", "salt", "pepper" }, new List<string> { }, "Sides", 20, "");
        public Item sausages = new Item("Sausages", "images/food/sausages.jpg", "Classic Sausages", 600, "4 standard size sausages", new List<string> { "pork" }, new List<string> { }, "Sides", 20, "");
        public Item bacon = new Item("Bacon", "images/food/bacon.jpg", "Classic Bacon", 600, "4 pieces of bacon", new List<string> { "pork" }, new List<string> { }, "Sides", 20, "");

        //Add On Items
        public Item apple = new Item("Apple", "images/food/apple.jpg", "", 400, "1 apple", new List<string> { "apple" }, new List<string> { }, "Add On Items", 5, "");
        public Item banana = new Item("Banana", "images/food/banana.jpg", "", 600, "1 waffle", new List<string> { "banana" }, new List<string> { }, "Add On Items", 5, "");
        public Item muffin = new Item("Muffin", "images/food/chocoMuffin.jpg", "Chocolate Chip Muffin", 600, "1 muffin", new List<string> { "flour", "sugar", "milk", "chocolate" }, new List<string> { "dairy" }, "Add On Items", 20, "");

        //Egg Breakfasts
        public Item omelette = new Item("Omelette", "images/food/omelette.jpg", "Delicious Cheese and Tomato omelette.", 500, "1 omelette", new List<string> { "eggs", "cheddar cheese", "mini tomatoes" }, new List<string> { "dairy" }, "Egg Breakfasts", 20, "");
        public Item scrambledEggs = new Item("Scrambled Eggs", "images/food/scrambledEggs.jpg", "Classic Scrambled Eggs", 600, "1 small plate full", new List<string> { "eggs" }, new List<string> { }, "Egg Breakfasts", 20, "");
        public Item sunnySideUpEggs = new Item("Sunny Side Up Eggs", "images/food/sunnysideEggs.jpg", "Classic Sunny Side Up Eggs", 600, "2 eggs", new List<string> { "eggs" }, new List<string> { }, "Egg Breakfasts", 20, "");

        //Sandwiches
        public Item breakfastBurrito = new Item("Breakfast Burrito", "images/food/breakfastBurrito.jpg", "Breakfast Burrito filled with eggs, tomato, sausage", 600, "1 burrito", new List<string> { "eggs", "tomatoes", "sausages", "bread" }, new List<string> { }, "Sandwiches", 20, "");
        public Item grilledCheese = new Item("Grilled Cheese", "images/food/grilledCheese.jpg", "Classic Grilled Cheese.", 600, "1 waffle", new List<string> { "flour", "sugar", "milk" }, new List<string> { "dairy" }, "Sandwiches", 20, "");
        public Item hamAndCheese = new Item("Ham and Cheese Sandwich", "images/food/hamCheeseSandwich.jpg", "Sandwich made with ham and cheese", 600, "1 sandwich", new List<string> { "bread", "ham", "cheese" }, new List<string> { }, "Sandwiches", 20, "");

        //Drinks
        public Item orangeJuice = new Item("Orange Juice", "images/food/oj.jpg", "Natural orange juice. Available with pulp and without.", 50, "1 glass", new List<string> { "oranges" }, new List<string> { }, "Drinks", 5, "");
        public Item appleJuice = new Item("Apple Juice", "images/food/apple_juice.jpg", "Natural apple juice.", 50, "1 glass", new List<string> { "apples" }, new List<string> { }, "Drinks", 5, "");
        public Item grapeJuice = new Item("Grape Juice", "images/food/grape_juice.jpg", "Natural grape juice.", 50, "1 glass", new List<string> { "grapes" }, new List<string> { }, "Drinks", 5, "");
        public Item peachJuice = new Item("Peach Juice", "images/food/peach_juice.jpg", "Natural peach juice.", 50, "1 glass", new List<string> { "peaches" }, new List<string> { }, "Drinks", 5, "");

        public Item coffee = new Item("Coffee", "images/food/coffee.jpg", "Coffee served with milk and sugar on the side.", 80, "1 cup", new List<string> { "coffee" }, new List<string> { "dairy" }, "Drinks", 7, "");
        public Item cappucino = new Item("Cappucino", "images/food/cappucino.jpg", "Classic Cappucino.", 90, "1 glass", new List<string> { "sugar", "coffee", "milk" }, new List<string> { "dairy" }, "Drinks", 5, "");
        public Item latte = new Item("Latte", "images/food/latte.jpg", "Classic Latte", 70, "1 glass", new List<string> { "milk", "coffee", "sugar" }, new List<string> { "dairy" }, "Drinks", 5, "");
        public Item hotChocolate = new Item("Hot Chocolate", "images/food/hot_chocolate.jpg", "Classic hot chocolate served with whipped cream and chocolate syrup", 200, "1 mug", new List<string> { "cocoa", "milk" }, new List<string> { "dairy" }, "Drinks", 5, "");


        public Menu()
        {

        }

        public List<Item> getAllMenuItems()
        {
            return new List<Item>()
            {
                originalPancakes,
                blueberryPancakes,
                originalWaffles,
                fruitParfait,
                hashBrowns,
                sausages,
                bacon,
                apple,
                banana,
                muffin,
                omelette,
                scrambledEggs,
                sunnySideUpEggs,
                breakfastBurrito,
                grilledCheese,
                hamAndCheese,
                orangeJuice,
                appleJuice,
                grapeJuice,
                peachJuice,
                coffee,
                cappucino,
                latte,
                hotChocolate
            };
        }

        public List<Item> getPancakesWaffles()
        {
            return new List<Item>()
            {
                originalPancakes,
                blueberryPancakes,
                originalWaffles
            };
        }

        public List<Item> getSides()
        {
            return new List<Item>()
            {
                fruitParfait,
                hashBrowns,
                sausages,
                bacon
            };
        }

        public List<Item> getAddOns()
        {
            return new List<Item>()
            {
                apple,
                banana,
                muffin
            };
        }

        public List<Item> getEggs()
        {
            return new List<Item>()
            {
                omelette,
                scrambledEggs,
                sunnySideUpEggs
            };
        }

        public List<Item> getSandwiches()
        {
            return new List<Item>()
            {
                breakfastBurrito,
                grilledCheese,
                hamAndCheese
            };
        }

        public List<Item> getDrinks()
        {
            return new List<Item>()
            {
                orangeJuice,
                appleJuice,
                grapeJuice,
                peachJuice,
                coffee,
                cappucino,
                latte,
                hotChocolate
            };
        }
    }
}
