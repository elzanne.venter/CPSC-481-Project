﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NewMoras
{



    public partial class PaymentScreen : ContentPage
    {
        static int child_amt;
        static int adult_amt;
        static int senior_amt;

        int child_unpaid;
        int adult_unpaid;
        int senior_unpaid;


        public PaymentScreen()
        {

            InitializeComponent();

            child_amt = App._amt_child;
            adult_amt = App._amt_adult;
            senior_amt = App._amt_senior;


            child_unpaid = child_amt;

            adult_unpaid = adult_amt;

            senior_unpaid = senior_amt;



            CreateCustomers();
            //foreach (Image im in myArray)
            //{
            //    stack.Children.Add(im);

            //}
            foreach (Button btn in myBtnArray)
            {
                btn.HeightRequest = 150;
                btn.WidthRequest = 150;
                maing.Children.Add(btn);
            }
            //maing.Children.Add(butn);
            updateText();
            this.ForceLayout();
        }


        int payment = -1;



        double price;

        int child_paying = 0;
        int adult_paying = 0;
        int senior_paying = 0;

        Image[] myArray = { };
        Button[] myBtnArray = { };

        void debit_button_Clicked(System.Object sender, System.EventArgs e)
        {
            credit_button.BackgroundColor = Color.White;
            cash_button.BackgroundColor = Color.White;
            debit_button.BackgroundColor = Color.FromHex("ceb1b1");
            method.TextColor = Color.Black;
            payment = 0;


        }

        void credit_button_Clicked(System.Object sender, System.EventArgs e)
        {
            credit_button.BackgroundColor = Color.FromHex("ceb1b1");
            cash_button.BackgroundColor = Color.White;
            debit_button.BackgroundColor = Color.White;
            method.TextColor = Color.Black;
            payment = 1;

        }

        void cash_button_Clicked(System.Object sender, System.EventArgs e)
        {
            credit_button.BackgroundColor = Color.White;
            cash_button.BackgroundColor = Color.FromHex("ceb1b1");
            debit_button.BackgroundColor = Color.White;
            method.TextColor = Color.Black;
            payment = 2;

        }


        public void CharacterClicked(System.Object sender, System.EventArgs e)
        {

            Button btn = sender as Button;

            foreach (Button im in myBtnArray)
            {
                string source = btn.ImageSource as FileImageSource;

                if (im.TranslationX == btn.TranslationX && im.TranslationY == btn.TranslationY)
                {
                    if (source == "images/child.png")
                    {
                        im.ImageSource = "images/child_green.png";
                        child_paying += 1;
                        price += 12.99;
                    }
                    else if (source == "images/child_green.png")
                    {
                        im.ImageSource = "images/child.png";
                        child_paying -= 1;
                        price -= 12.99;
                    }

                    else if (source == "images/adult.png")
                    {
                        im.ImageSource = "images/adult_green.png";
                        adult_paying += 1;
                        price += 15.99;
                    }
                    else if (source == "images/adult_green.png")
                    {
                        im.ImageSource = "images/adult.png";
                        adult_paying -= 1;
                        price -= 15.99;
                    }

                    else if (source == "images/senior.png")
                    {
                        im.ImageSource = "images/senior_green.png";
                        senior_paying += 1;
                        price += 10.99;
                    }
                    else if (source == "images/senior_green.png")
                    {
                        im.ImageSource = "images/senior.png";
                        senior_paying -= 1;
                        price -= 10.99;
                    }
                }
            }
            updateText();
        }



        public void CreateCustomers()
        {
            int ind = 0;
            double hyp = 225.0;

            int temp_child_unpaid = child_unpaid;
            int temp_adult_unpaid = adult_unpaid;
            int temp_senior_unpaid = senior_unpaid;

            Button button;


            List<string> customers = new List<string>(); ;

            int i = 0;
            while (i < child_amt)
            {
                customers.Add("child");
                i++;
            }
            i = 0;
            while (i < adult_amt)
            {
                customers.Add("adult");
                i++;
            }
            i = 0;
            while (i < senior_amt)
            {
                customers.Add("senior");
                i++;
            }



            foreach (string customer in customers)
            {

                double rad = (((2 * Math.PI) / customers.Count) * ind);
                double opp = Math.Sin(rad) * hyp + 290;
                Console.WriteLine(opp);
                double adj = (Math.Cos(rad) * hyp) + 940;


                string source = "images/" + customer + ".png";



                button = new Button
                {
                    ImageSource = source,

                    HeightRequest = 100,
                    WidthRequest = 100,

                    TranslationX = adj,
                    TranslationY = opp,
                    Padding = 0,




                };


                button.Clicked += CharacterClicked;


                Array.Resize(ref myBtnArray, myBtnArray.Length + 1);
                myBtnArray[myBtnArray.GetUpperBound(0)] = button;

                ind += 1;
            }
            //Randomize(myBtnArray);
        }

        public void submit_pay(System.Object sender, System.EventArgs e)
        {

            if (child_paying == 0 && adult_paying == 0 && senior_paying == 0)
            {
                error_msg.Text = "Please Select Person(s) and Payment Method";

            }
            else if (payment == -1)
            {
                error_msg.Text = "Please Select Person(s) and Payment Method";
            }
            else if (child_paying != child_unpaid || adult_paying != adult_unpaid || senior_paying != senior_unpaid)
            {

                foreach (Button imButton in myBtnArray)
                {
                    string psource = imButton.ImageSource as FileImageSource;  //Getting the name of source as string
                    if (psource == "images/child_green.png")
                    {
                        imButton.ImageSource = "images/child_blurred.png";
                    }
                    if (psource == "images/adult_green.png")
                    {
                        imButton.ImageSource = "images/adult_blurred.png";
                    }
                    if (psource == "images/senior_green.png")
                    {
                        imButton.ImageSource = "images/senior_blurred.png";
                    }
                }


                child_unpaid -= child_paying;
                adult_unpaid -= adult_paying;
                senior_unpaid -= senior_paying;
                method.TextColor = Color.Black;


                credit_button.BackgroundColor = Color.White;
                cash_button.BackgroundColor = Color.White;
                debit_button.BackgroundColor = Color.White;
                payment = -1;

                child_paying = 0;
                adult_paying = 0;
                senior_paying = 0;
                price = 0;

                updateText();

            }
            else
            {
                endScreen(sender, e);


            }
        }

        public void updateText()
        {
            children.Text = child_amt != 0 ? child_paying + "/" + child_unpaid + " children" : "";
            adults.Text = adult_amt != 0 ? adult_paying + "/" + adult_unpaid + " adults" : "";
            seniors.Text = senior_amt != 0 ? senior_paying + "/" + senior_unpaid + " seniors" : "";

            if (price < 5)
            {
                price = 0;
            }
            amount.Text = "$" + price.ToString();
        }
        public static void Randomize<T>(T[] items)
        {
            Random rand = new Random();

            // For each spot in the array, pick
            // a random item to swap into that spot.
            for (int i = 0; i < items.Length - 1; i++)
            {
                int j = rand.Next(i, items.Length);
                T temp = items[i];
                items[i] = items[j];
                items[j] = temp;
            }
        }
        private async void endScreen(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EndScreen());
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
        }
    }





}

