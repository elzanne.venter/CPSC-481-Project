﻿using System;
using System.Collections.Generic;
namespace NewMoras
{
    public class Item
    {

        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public int Calories { get; set; }
        public string ServingSize { get; set; }
        public List<string> Ingredients { get; set; }
        public List<string> Allergens { get; set; }
        public string Category { get; set; }
        public int EstimatedPrepTime { get; set; }
        public string Comments { get; set; }
        public bool Delivered { get; set; }


        public Item()
        {
            Delivered = false;
        }

        public Item(string itemname, string image, string descript, int cal, string servsize, List<string> ingred, List<string> all, string cat, int estimate_prep, string comments)
        {
            Name = itemname;
            ImageUrl = image;
            Description = descript;
            Calories = cal;
            ServingSize = servsize;
            Ingredients = ingred;
            Allergens = all;
            Category = cat;
            EstimatedPrepTime = estimate_prep;
            Comments = comments;
            Delivered = false;

        }


    }
}