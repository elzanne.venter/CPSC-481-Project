﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NewMoras
{
    public partial class App : Application
    {

        public static int _amt_child = 0;
        public static int _amt_adult = 0;
        public static int _amt_senior = 0;


        public NavigationPage OrderScreen { get; }
        public NavigationPage PaymentScreen { get; }
        public NavigationPage ItemScreen { get; }

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new NewMoras.MainPage())
            {
                BarBackgroundColor = Color.FromHex("#ceb1b1"),
                BarTextColor = Color.FromHex("#332c2c")
            };
            

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
        
    }
}
