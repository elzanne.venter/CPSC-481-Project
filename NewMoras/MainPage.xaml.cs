﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NewMoras
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
           

            InitializeComponent();

            App._amt_adult = 0;
            App._amt_child = 0;
            App._amt_senior = 0;

        }

        private async void Guest_Screen(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GuestScreen());
        }

    }
}
