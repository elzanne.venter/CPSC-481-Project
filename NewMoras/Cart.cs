﻿using System;
using System.Collections.Generic;
namespace NewMoras
{
    public class Cart
    {
        public int item_id = 0; //when you add selectedItems MUST increment item_id immediately after
        public Dictionary<string, int> quantities;
        public Dictionary<int, Item> selectedItems;

        public Cart()
        {
        }
    }
}
