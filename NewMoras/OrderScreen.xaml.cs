﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NewMoras
{
    public partial class OrderScreen : ContentPage
    {
        public bool isServerAnimating { get; set; }
     
        public Cart newCart = new Cart();
        public Dictionary<int, Item> inProgressItems = new Dictionary<int, Item>();
        public Dictionary<int, string> DeliveredItems = new Dictionary<int, string>();

        




        public OrderScreen()
        {
            InitializeComponent();

            isServerAnimating = false;

            


            var cartTapRecognizer = new TapGestureRecognizer();
            cartTapRecognizer.Tapped += async (s, e) => {
                // handle the tap

                information_label.BackgroundColor = Color.White;
                information_label.TextColor = Color.Black;

                cart_label.BackgroundColor = Color.FromHex("#AF7287");
                cart_label.TextColor = Color.White;

                server_label.BackgroundColor = Color.White;
                server_label.TextColor = Color.Black;

                cart_screen.IsVisible = true;
                information_screen.IsVisible = false;
                server_screen.IsVisible = false;


            };

            cart_label.GestureRecognizers.Add(cartTapRecognizer);

            var informationTapRecognizer = new TapGestureRecognizer();
            informationTapRecognizer.Tapped += async (s, e) => {
                // handle the tap
                cart_label.BackgroundColor = Color.White;
                cart_label.TextColor = Color.Black;

                information_label.BackgroundColor = Color.FromHex("#AF7287");
                information_label.TextColor = Color.White;

                server_label.BackgroundColor = Color.White;
                server_label.TextColor = Color.Black;

                cart_screen.IsVisible = false;
                information_screen.IsVisible = true;
                server_screen.IsVisible = false;


            };
            information_label.GestureRecognizers.Add(informationTapRecognizer);

            var serverTapRecognizer = new TapGestureRecognizer();
            serverTapRecognizer.Tapped += async (s, e) => {
                // handle the tap
                cart_label.BackgroundColor = Color.White;
                cart_label.TextColor = Color.Black;

                information_label.BackgroundColor = Color.White;
                information_label.TextColor = Color.Black;

                server_label.BackgroundColor = Color.FromHex("#AF7287");
                server_label.TextColor = Color.White;

                cart_screen.IsVisible = false;
                information_screen.IsVisible = false;
                server_screen.IsVisible = true;
            };
            server_label.GestureRecognizers.Add(serverTapRecognizer);

            newCart.quantities = new Dictionary<string, int>();
            newCart.selectedItems = new Dictionary<int, Item>();

            orders_lists.HeightRequest = 0;
            in_progress_orders.HeightRequest = in_progress_orders.RowHeight;
            delivered_orders.HeightRequest = delivered_orders.RowHeight;

        }

        private async void Payment_Screen(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaymentScreen());
        }

        private async void QuestionGesture(object sender, EventArgs e)
        {
            if (!isServerAnimating)
            {
                isServerAnimating = true;
                speechBubble.TranslateTo(0, -100, 1); //No need to wait for this action, it will complete while the server is animating.
                await serverImage.TranslateTo(-100, 20, 700, Easing.CubicInOut);
                await speechBubble.FadeTo(1.0, 350, Easing.CubicInOut);
            }
        }

        private async void OnSpeechBubbleTapped(object sender, EventArgs args)
        {
            try
            {
                if (isServerAnimating)
                {
                    await speechBubble.FadeTo(0, 350, Easing.CubicIn);
                    await serverImage.TranslateTo(0, 0, 700, Easing.CubicInOut);
                    speechBubble.TranslateTo(0, -100, 1);
                    isServerAnimating = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async void Item_Screen(object sender, EventArgs e)
        {
            //uses the Image Source (the ImageURL) to find the right menu item
            string itemTemp = (((Image)sender).Source).ToString();

            //have to split the string because it returns as 'File: image/food/x.png' and we only need the second half
            string[] strlist = itemTemp.Split(' ');
            string url = strlist[1];

            Menu newMenu = new Menu();
            List<Item> allItems = newMenu.getAllMenuItems();

            //iterate through all menu items to find the right image that was clicked 
            foreach (Item i in allItems)
            {

                if (url == i.ImageUrl)
                {
                    infoName.Text = i.Name;
                    infoImage.Source = i.ImageUrl;
                    infoDesc.Text = i.Description;
                    infoCal.Text = (i.Calories).ToString() + " calories";
                    infoSize.Text = i.ServingSize;

                    //converts ingredients and allergens lists to be displayed as strings separating each item with a comma
                    string ingrResult = string.Join(", ", (i.Ingredients).ToArray());
                    infoIng.Text = ingrResult;

                    string allerResult = string.Join(", ", (i.Allergens).ToArray());
                    infoAller.Text = allerResult;
                }

            }

            cart_label.BackgroundColor = Color.White;
            cart_label.TextColor = Color.Black;

            information_label.BackgroundColor = Color.FromHex("#AF7287");
            information_label.TextColor = Color.White;

            server_label.BackgroundColor = Color.White;
            server_label.TextColor = Color.Black;

            cart_screen.IsVisible = false;
            information_screen.IsVisible = true;
            server_screen.IsVisible = false;
        }


        private async void OrderGesture(System.Object sender, EventArgs e)
        {
            Order order = new Order(newCart);
            Dictionary<int, string> ItemNameTemp = new Dictionary<int, string>();


            foreach (var i in order.selectedItems)
            {
                ItemNameTemp.Add(i.Key, i.Value.Name);
                inProgressItems.Add(i.Key, i.Value);
            }

            in_progress_orders.ItemsSource = null;
            in_progress_orders.ItemsSource = ItemNameTemp;

            in_progress_orders.HeightRequest = in_progress_orders.RowHeight * inProgressItems.Count;


            newCart.quantities.Clear();
            orders_lists.ItemsSource = null;
            orders_lists.ItemsSource = newCart.quantities;

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;

            textBox.Text = null; 

            pancakesAmt.Text = "0";
            bPancakesAmt.Text = "0";
            wafflesAmt.Text = "0";

            burritoAmt.Text = "0";
            hamCheeseAmt.Text = "0";
            grilledCheeseAmt.Text = "0";

            omeletteAmt.Text = "0";
            sunnysideEggsAmt.Text = "0";
            scrambledEggsAmt.Text = "0";

            appleAmt.Text = "0";
            bananaAmt.Text = "0";
            muffinAmt.Text = "0";

            fruitParfaitAmt.Text = "0";
            HashBrownsAmt.Text = "0";
            SausagesAmt.Text = "0";
            BaconAmt.Text = "0";

            OrangeJuiceAmt.Text = "0";
            AppleJuiceAmt.Text = "0";
            GrapeJuiceAmt.Text = "0";
            PeachJuiceAmt.Text = "0";
            CoffeeAmt.Text = "0";
            CappucinoAmt.Text = "0";
            LatteAmt.Text = "0";
            HotChocolateAmt.Text = "0";




        }

        void RefreshStatus(object sender, EventArgs e)
        {

            Dictionary<int, string> tempInProgressItems = new Dictionary<int, string>();
            foreach (var i in inProgressItems)
            {
                if (i.Value.Delivered)
                {
                    DeliveredItems.Add(i.Key, i.Value.Name);

                }
            }

            foreach (var i in DeliveredItems)
            {
                inProgressItems.Remove(i.Key);
            }

            foreach (var i in inProgressItems)
            {
                tempInProgressItems.Add(i.Key, i.Value.Name);

            }

            in_progress_orders.ItemsSource = null;
            in_progress_orders.ItemsSource = tempInProgressItems;



            delivered_orders.ItemsSource = null;
            delivered_orders.ItemsSource = DeliveredItems;

            in_progress_orders.HeightRequest = in_progress_orders.RowHeight * tempInProgressItems.Count;
            delivered_orders.HeightRequest = delivered_orders.RowHeight * DeliveredItems.Count;




        }



        /**
        ADD AND REMOVE METHODS FOR PANCAKES AND WAFFLES
             **/
        void Add_Pancakes(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                     
                    }

                }
                pancakesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                pancakesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Pancakes(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    pancakesAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_BPancakes(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                bPancakesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                bPancakesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_BPancakes(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    bPancakesAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Waffles(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                wafflesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                wafflesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Waffles(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    wafflesAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        /**
       ADD AND REMOVE METHODS FOR SANDWICHES
            **/

        void Add_Burrito(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                burritoAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                burritoAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Burrito(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    burritoAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_GrilledCheese(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                grilledCheeseAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                grilledCheeseAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_GrilledCheese(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    grilledCheeseAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_HamCheese(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                hamCheeseAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                hamCheeseAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_HamCheese(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    hamCheeseAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }


        /**
       ADD AND REMOVE METHODS FOR EGG BREAKFASTS
            **/
        void Add_Omelette(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                omeletteAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                omeletteAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Omelette(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    omeletteAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_ScrambledEggs(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                scrambledEggsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                scrambledEggsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_ScrambledEggs(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    scrambledEggsAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_SunnysideEggs(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                sunnysideEggsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                sunnysideEggsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_SunnysideEggs(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    sunnysideEggsAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        /**
       ADD AND REMOVE METHODS FOR ADD ONS
            **/
        void Add_Apple(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                appleAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                appleAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Apple(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    appleAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }

               
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Banana(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                bananaAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                bananaAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Banana(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    bananaAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Muffin(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                muffinAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                muffinAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Muffin(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    muffinAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        /**
       ADD AND REMOVE METHODS FOR SIDES
            **/

        void Add_FruitParfait(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                fruitParfaitAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                fruitParfaitAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_FruitParfait(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    fruitParfaitAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }
            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_HashBrowns(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                HashBrownsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                HashBrownsAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_HashBrowns(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    HashBrownsAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Sausages(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                SausagesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                SausagesAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Sausages(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    SausagesAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Bacon(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                BaconAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                BaconAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Bacon(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    BaconAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        /**
       ADD AND REMOVE METHODS FOR DRINKS
            **/
        void Add_OrangeJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                OrangeJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                OrangeJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_OrangeJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    OrangeJuiceAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_AppleJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                AppleJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                AppleJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_AppleJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    AppleJuiceAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_GrapeJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                GrapeJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                GrapeJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_GrapeJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    GrapeJuiceAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_PeachJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                PeachJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                PeachJuiceAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_PeachJuice(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    PeachJuiceAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Coffee(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                CoffeeAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                CoffeeAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Coffee(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    CoffeeAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Cappucino(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                CappucinoAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                CappucinoAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Cappucino(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    CappucinoAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_Latte(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                LatteAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                LatteAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_Latte(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    LatteAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Add_HotChocolate(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            if ((newCart.quantities).ContainsKey(itemName))
            {
                newCart.item_id++;
                newCart.quantities[itemName] += 1;

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        newCart.selectedItems.Add(newCart.item_id, i);
                    }

                }
                HotChocolateAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }
            else
            {
                newCart.item_id++;
                newCart.quantities.Add(itemName, 1);

                Menu newMenu = new Menu();
                List<Item> allItems = newMenu.getAllMenuItems();
                foreach (Item i in allItems)
                {

                    if (itemName == i.Name)
                    {
                        int temp = newCart.item_id;
                        newCart.selectedItems.Add(temp, i);
                        break;
                    }



                }
                HotChocolateAmt.Text = newCart.quantities[itemName].ToString();
                orders_lists.ItemsSource = null;
                orders_lists.ItemsSource = newCart.quantities;
            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }

        void Remove_HotChocolate(object sender, EventArgs e)
        {
            string itemTemp = ((Button)sender).BindingContext as string;
            string itemName = itemTemp.ToString();

            foreach (var i in newCart.selectedItems)
            {

                if (itemName == i.Value.Name)
                {
                    int currentKey = i.Key;
                    newCart.selectedItems.Remove(currentKey);
                    break;
                }

            }

            if (newCart.quantities.ContainsKey(itemName))
            {
                if (newCart.quantities[itemName] > 0)
                {
                    newCart.quantities[itemName] -= 1;
                    HotChocolateAmt.Text = newCart.quantities[itemName].ToString();

                    if (newCart.quantities[itemName] == 0)
                    {
                        newCart.quantities.Remove(itemName);
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                    else
                    {
                        orders_lists.ItemsSource = null;
                        orders_lists.ItemsSource = newCart.quantities;
                    }
                }
                else
                {
                    orders_lists.ItemsSource = newCart.quantities;

                }
            }
            else
            {
                orders_lists.ItemsSource = newCart.quantities;

            }

            orders_lists.HeightRequest = orders_lists.RowHeight * newCart.quantities.Count;
        }
    }

}