﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace NewMoras
{
    public partial class GuestScreen : ContentPage
    {
        public GuestScreen()
        {
            InitializeComponent();
        }
        int amt_chd = 0;
        int amt_adt = 0;
        int amt_snr = 0;

        void Minus_Child(System.Object sender, System.EventArgs e)
        {
            if (amt_chd == 0)
            {

            }
            else
            {
                amt_chd -= 1;
                ChdAmt.Text = amt_chd.ToString();
            }
        }
        void Add_Child(System.Object sender, System.EventArgs e)
        {


            amt_chd += 1;
            ChdAmt.Text = amt_chd.ToString();

        }


        void Minus_Adult(System.Object sender, System.EventArgs e)
        {
            if (amt_adt == 0)
            {

            }
            else
            {
                amt_adt -= 1;
                AdtAmt.Text = amt_adt.ToString();
            }
        }
        void Add_Adult(System.Object sender, System.EventArgs e)
        {


            amt_adt += 1;
            AdtAmt.Text = amt_adt.ToString();

        }


        void Minus_Senior(System.Object sender, System.EventArgs e)
        {
            if(amt_snr == 0)
            {
                
            }
            else
            {
                amt_snr -= 1;
                SnrAmt.Text = amt_snr.ToString();
            }
        }
        void Add_Senior(System.Object sender, System.EventArgs e)
        {
            
            
            amt_snr += 1;
            SnrAmt.Text = amt_snr.ToString();
            
        }
        private async void Order_Screen(object sender, EventArgs e)
        {
            if (amt_adt == 0 && amt_chd == 0 && amt_snr == 0)
            {
                error_label.Text = "Please Enter Who You Are Dining With";
                return;
            }
            else
            {
                if (error_label.Text == "Please Enter Who You Are Dining With")
                {
                    error_label.Text = "";
                }

                else
                {

                }

                App._amt_adult = amt_adt;
                App._amt_child = amt_chd;
                App._amt_senior = amt_snr;
                await Navigation.PushAsync(new OrderScreen());
            }
     
        }
    }
}
